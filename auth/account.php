<? include_once '../system/init.php';
   include_once 'authClass.php';
?>
<!DOCTYPE html>
<html lang="<?=$loc->lang?>">
<head>
    <meta charset="utf-8"/>
    <title><?=$loc->data->i[0]?></title>
    <meta name="viewport" content="width=device-width">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
    <?php include("../js/analitics.php"); ?>
    <link rel="stylesheet" href="css/nav.css"/>
    <link rel="stylesheet" href="css/authForm.css"/>
    <style type="text/css">
        .border { border-color: red !important; }
        .dotted{ border:2px dotted red; }
        .nav-brand {margin-top:30px;}
        #passMemory { color:#47a; font-size:16px; }
        #passMemory:hover { color:#19a; }
        #exampleInputEmail2 {padding: 5px !important;}
        .informer, #emailRecheck { display: none; opacity:0.0; }
        .main-box {margin-top:70px;}
    </style>
    <? include("../block/options.php");?>
<body>
<section class="hbox stretch">
    <section id="content">
        <section class="vbox">
          <div id="slider-box">
            <? include_once 'head.inc.php'; ?>
          </div>
          <section class="scrollable wrapper">
                <div class="col-lg-12">

                    <div class="col-lg-4 col-md-6 col-xs-12 col-lg-offset-4 col-md-offset-3 m-t-lg main-box">
                        <section class="panel" id="authForm">
                            <header class="panel-heading text-center"><?=$loc->data->reg->i[0]?></header>

                            <form action="" class="panel-body" name="loginform" id="loginform">

                                <div class="form-group">
                                    <label class="control-label"><?=$loc->data->reg->i[1]?></label>
                                    <input type="email" required="required" placeholder="zenyk@example.com" class="form-control" id="email"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label"><?=$loc->data->reg->i[2]?></label>
                                    <input type="password" required="required" id="password" placeholder="zenykParoler" class="form-control"/>
                                </div>

                                <a href="" id="passMemory" class="pull-right m-t-xs">
                                    <small><?=$loc->data->reg->i[3]?></small>
                                </a>

                                <button type="submit" class="btn btn-info" id="login-button"><?=$loc->data->reg->i[0]?></button>



                                <span class="auth-desc"><?=$loc->data->log->i[4]?></span>
                                <div class="socio-box">
                                  <a class="socio-auth" href="socialAuthVk.php"><i class="i icon-vk"></i></a>
                                  <a class="socio-auth" href="socialAuthFacebook.php"><i class="i icon-facebook"></i></a>
                                  <a class="socio-auth" href="socialAuthGoogle.php"><i class="i icon-google-plus"></i></a>
                                  <a class="socio-auth" href="socialAuthGit.php"><i class="i icon-github"></i></a>
                                  <a class="socio-auth" href="socialAuthTw.php"><i class="i icon-twitter"></i></a>
                                  <a class="socio-auth" href="socialAuthLinkedIn.php"><i class="i icon-linkedin"></i></a>
                                </div>

                                <p class="text-muted text-center">
                                    <small><?=$loc->data->reg->i[10]?></small>
                                </p>
                                <a href="registration" class="btn btn-white btn-block"><?=$loc->data->head->i[2]?></a>
                            </form>

                            <form class="panel-body" name="emailRecheck" id="emailRecheck">

                                <div id="unf" class="alert alert-danger informer">
                                  <button type="button" class="close" data-dismiss="alert"><i class="fa icon-remove"></i></button>
                                  <strong><?=$loc->data->alert->i[0]?></strong> <a href="#" class="alert-link"><?=$loc->data->alert->i[1]?></a>
                                </div>

                                <div id="uns" class="alert alert-danger informer">
                                  <button type="button" class="close" data-dismiss="alert"><i class="fa icon-remove"></i></button>
                                  <strong><?=$loc->data->alert->i[4]?></strong> <a href="#" class="alert-link"><?=$loc->data->alert->i[5]?></a>
                                </div>

                                <div id="uok" class="alert alert-success informer">
                                  <button type="button" class="close" data-dismiss="alert"><i class="fa icon-remove"></i></button>
                                  <strong><?=$loc->data->alert->i[2]?></strong> <?=$loc->data->alert->i[3]?>
                                </div>

                                <div class="form-group">
                                  <label class="rec-info control-label"><?=$loc->data->recover->i[0]?></label>
                                  <input type="email" required="required" placeholder="zenyk@example.com" class="form-control" id="recovery-value"/>
                                </div>

                                <button type="submit" class="btn btn-info" id="rechek-button"><?=$loc->data->recover->i[1]?></button>
                                <button type="button" class="btn btn-success" style="float:right;" id="cancel-recheck"><?=$loc->data->recover->i[2]?></button>
                            </form>
                        </section>
                    </div>

                </div>
            </section>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>

<? if(isset($_GET['i'])) $core->toJS($_GET['i'],'ini_action'); ?>
<script src="../css/app.v2.js"></script>
<? $core->toJs($loc->data->js,'loc'); ?>
<script src="script/login.js"></script>
<? if(isset($_SESSION['login_er'])):?>
<script type="text/javascript">
$(function(){
    $('#authForm').delay(500).animate({'marginTop':100,'opacity':0.7},550,'easeOutBounce',function(){
      $("#email,#password").val('').addClass('border');
    }).animate({'marginTop':0,'opacity':1.0},600,'easeOutBounce');
});
</script>
<? unset($_SESSION['login_er']); endif;?>
</body>
</html>
