<?php
$client_id = '189217001288226'; // Client ID
$client_secret = '67642712757e445d16e6d7b4e591575a'; // Client secret
$redirect_uri = 'http://devbattles.com/auth/fb.php'; // Redirect URIs

$url = 'https://www.facebook.com/dialog/oauth';

$params = array(
    'client_id'     => $client_id,
    'redirect_uri'  => $redirect_uri,
    'response_type' => 'code',
    'scope'         => 'email,public_profile,publish_actions'
);

//echo $link = '';
header('Refresh: 0; URL="'.$url.'?'.urldecode(http_build_query($params)).'"');

include_once("authClass.php");

if (isset($_GET['code'])) {
    $result = false;

    $params = array(
      'client_id'     => $client_id,
      'redirect_uri'  => $redirect_uri,
      'client_secret' => $client_secret,
      'code'          => $_GET['code']
    );

    $url = 'https://graph.facebook.com/oauth/access_token';

    $tokenInfo = null;
    parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

    if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
      $params = array('access_token' => $tokenInfo['access_token']);
      $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params))), true);
      if (isset($userInfo['id'])){

        $userInfo = $userInfo;
        $result = true;

        // get long-access token
        $longToken = parse_str( file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=$client_id&client_secret=$client_secret&grant_type=fb_exchange_token&fb_exchange_token=".$tokenInfo['access_token']) );
        $auth->testRes($longToken);

      }
    }

    if ($result) {
        $auth->data['name'] =  $userInfo['name'].' '.$userInfo['surname'];
        $auth->data['email'] = $userInfo['email'];
        $auth->data['img'] = 'http://graph.facebook.com/'.$userInfo['id'].'/picture?type=large';
        $auth->data['date'] = date("Y-m-d");
        $auth->data['facebook'] = $userInfo['id'];

        $auth->socAuth('facebook',$userInfo['id']);
    }
}
function toArray($query){
    $res = array();
    while($row = mysql_fetch_assoc($query)){
        $res[] = $row;
    }
    return $res;
}

?>
