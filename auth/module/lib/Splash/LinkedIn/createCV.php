<?php

include_once __DIR__.'/../../../../../system/crud.php';

class CreateCV extends crud {

  public function __construct($db = ['db_pref'=>'ru','prevent'=>'ru']){
    parent::__construct($db);
    $this->test = 1;
  }

  public function checkLangType($name){
    $a = [
      'Proficiency...'=>1,
      'Elementary proficiency'=>0,
      'Limited working proficiency'=>1,
      'Professional working proficiency'=>1,
      'Full professional proficiency'=>2,
      'Native or bilingual proficiency'=>2
    ];
    return isset($a[$name]) ? $a[$name] : 1;
  }

  /**
  * create CV if not exists
  */
  public function create( $user_id, $url ){
    # test cv exists
    $q = $this->getData('resumes',['id'],['user_id'=>$user_id],[],1);
    if(!isset($q[0]['id'])){

      $user_info = $this->getData('registerusers',['country'],['id'=>$user_id],[],1);

      # insert to DB CV props
      $cv = [
        'user_id'=>$user_id,
        'expirience'=>rand(1,3),
        'city'=>'',
        'country'=>$user_info[0]['country'],
        'expects'=>'',
        'type'=>1,
        'date'=>date('Y-m-d H:i:s'),
        'position'=>'',
        'views'=>1
      ];

      $salary = rand(8,15);
      $salary = $salary>10 ? $salary*100 : $salary+1000;
      $cv['salary']=$salary;

      include_once __DIR__."/parse.php";
      $ParseLinkedIn = new ParseLinkedIn;
      $data = $ParseLinkedIn->parse( $url );
      if(isset($data->skills)){

        # set country
        if(isset($data->country)){
          // $country = $this->getData('country', ['id'], ['name'=>$data->country],[],1);
          // if(isset($country[0]['id'])){
          //   $cv->country = $country[0]['id'];
          //   $this->updateData('registerusers',['id'=>$user_id],['country'=>$country[0]['id']],1);
          // }
        }

        # export user skills
        if(isset($data->skills)){
          for($i=0,$c=count($data->skills); $i<$c;$i++){
            # get skills id from table
            $id = $this->getData('special_catlist',['id'],[ "name"=>$data->skills[$i] ],[],1);
            if(isset($id[0]['id'])){
              $id = $id[0]['id'];
            } else {
              $id = $this->insertData('special_catlist',['name'=>$data->skills[$i]]);
            }
            # create user skill
            $this->insertData('user_skills',['user_id'=>$user_id,'skill_id'=>$id]);
          }
        }

        # interests
        if(isset($data->interest))
          $this->updateData('registerusers',['id'=>$user_id],['interestings'=>$data->interest],[],1);

        #create work
        $work = "";
        if(isset($data->position)){
          for($i=0,$c=count($data->position); $i<$c;$i++){
            $work .= $data->position[$i]['post']."\n".$data->position[$i]['where']."\n".$data->position[$i]['start']." - ".$data->position[$i]['complete']."\n\n".$data->position[$i]['desc']."\n\n\n";
          }
        }
        if(isset($data->publications)){
          $work .="\n";
          for($i=0,$c=count($data->publications); $i<$c;$i++){
            $work .= $data->publications[$i]['title']."\n".$data->publications[$i]['where']."\n".$data->publications[$i]['authors']." - ".$data->publications[$i]['text']."\n\n";
          }
        }

        $cv['work'] = $work;

        # education
        $edu = [];
        for($i=0,$c=count($data->education); $i<$c;$i++){
          $edu[$i] = new stdClass;
          $edu[$i]->name = $data->education[$i]['title'];
          $edu[$i]->start = $data->education[$i]['start'];
          $edu[$i]->complete = $data->education[$i]['complete'];
        }
        $edu = $this->json_encode_cyr($edu);
        $cv['education'] = $edu;

        # language
        $lang = [];
        for($i=0,$c=count($data->language); $i<$c;$i++){
          $lang[$i] = new stdClass;
          $lang[$i]->name = $data->language[$i]['name'];
          $lang[$i]->type = $this->checkLangType( $data->language[$i]['level'] );
        }
        $lang = $this->json_encode_cyr($lang);
        $cv['lang'] = $lang;

        try {
          $id = $this->insertData('resume',$cv);
          if($id) return $id;
        } catch(Error $e){

        }
      }
    }
  }

  public function json_encode_cyr($inObj) {
    $trans = array(
      '\u0430'=>'а', '\u0431'=>'б', '\u0432'=>'в', '\u0433'=>'г',
      '\u0434'=>'д', '\u0435'=>'е', '\u0451'=>'ё', '\u0436'=>'ж',
      '\u0437'=>'з', '\u0438'=>'и', '\u0439'=>'й', '\u043a'=>'к',
      '\u043b'=>'л', '\u043c'=>'м', '\u043d'=>'н', '\u043e'=>'о',
      '\u043f'=>'п', '\u0440'=>'р', '\u0441'=>'с', '\u0442'=>'т',
      '\u0443'=>'у', '\u0444'=>'ф', '\u0445'=>'х', '\u0446'=>'ц',
      '\u0447'=>'ч', '\u0448'=>'ш', '\u0449'=>'щ', '\u044a'=>'ъ',
      '\u044b'=>'ы', '\u044c'=>'ь', '\u044d'=>'э', '\u044e'=>'ю',
      '\u044f'=>'я',
      '\u0410'=>'А', '\u0411'=>'Б', '\u0412'=>'В', '\u0413'=>'Г',
      '\u0414'=>'Д', '\u0415'=>'Е', '\u0401'=>'Ё', '\u0416'=>'Ж',
      '\u0417'=>'З', '\u0418'=>'И', '\u0419'=>'Й', '\u041a'=>'К',
      '\u041b'=>'Л', '\u041c'=>'М', '\u041d'=>'Н', '\u041e'=>'О',
      '\u041f'=>'П', '\u0420'=>'Р', '\u0421'=>'С', '\u0422'=>'Т',
      '\u0423'=>'У', '\u0424'=>'Ф', '\u0425'=>'Х', '\u0426'=>'Ц',
      '\u0427'=>'Ч', '\u0428'=>'Ш', '\u0429'=>'Щ', '\u042a'=>'Ъ',
      '\u042b'=>'Ы', '\u042c'=>'Ь', '\u042d'=>'Э', '\u042e'=>'Ю',
      '\u042f'=>'Я',
      '\u0456'=>'і', '\u0406'=>'І', '\u0454'=>'є', '\u0404'=>'Є',
      '\u0457'=>'ї', '\u0407'=>'Ї', '\u0491'=>'ґ', '\u0490'=>'Ґ'
    );
    return strtr( json_encode($inObj), $trans);
  }

  public function

}

$CreateCV = new CreateCV;
if(isset($_POST['f59i_d844kg_xt']) && $_POST['f59i_d844kg_xt'] == md5(sha1('n%54i95xtsfisdkfsdfcdk548j3cdsdfdf'))){
  $CreateCV->create();
}
