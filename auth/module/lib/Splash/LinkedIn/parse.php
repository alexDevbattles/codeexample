<?php

class ParseLinkedIn {

  public function __construct(){}

  public function clear($str){
    return mysql_real_escape_string (str_replace(["`","'",'"'], [''], $str));
  }

  public function parse($url){
    $res = new stdClass;

    # get page
    $data = '';
    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_URL, $url);
    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 3);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0');
    $data = curl_exec($curl_handle);
    curl_close($curl_handle);


    require __DIR__.'/../../../../../system/helper/core/phpQuery/phpQuery.php';
    try {
      $doc = phpQuery::newDocument(mb_convert_encoding($data, 'HTML-ENTITIES', "UTF-8"));
      # country
      foreach(pq('span.locality') as $q){
        $res->country = $q->nodeValue;
      }

      # skills
      $skills = [];
      foreach(pq('li.skill') as $q){
        if(trim($q->textContent)!='See less')
          array_push($skills, $this->clear( trim($q->textContent)) );
      }
      if( !empty($skills) ) $res->skills = $skills;

      # education
      $education = [];
      foreach(pq('li.school') as $q){
        $name = explode(',',trim($q->childNodes[0]->textContent));
        $year = explode(' – ',$q->childNodes[1]->textContent);
        if(isset($name[0])){
          $tmp = [
            'start'=> isset($year[0]) ? $year[0] : '',
            'complete'=> isset($year[1]) ? $year[1] : '',
            'title'=> $this->clear( $name[0] ),
            'degre'=> isset($name[1]) ? $this->clear( $name[1] ) : '',
          ];
          array_push($education, $tmp);
        }
      }
      if( !empty($education) ) $res->education = $education;

      # position
      $position = [];
      foreach(pq('li.position') as $q){
        $year = explode(' – ',$q->childNodes[1]->childNodes[0]->textContent);
        if(isset($q->childNodes[0]->childNodes[1]->textContent)){
          $tmp = [
            'start'=> isset($year[0]) ? $year[0] : '',
            'complete'=> isset($year[1]) ? $year[1] : '',
            'post'=> isset($q->childNodes[0]->childNodes[1]->textContent) ? $this->clear( $q->childNodes[0]->childNodes[1]->textContent ) : '',
            'where'=> isset($q->childNodes[0]->childNodes[2]->childNodes[0]->textContent) ? $this->clear( $q->childNodes[0]->childNodes[2]->childNodes[0]->textContent ) : '',
            'desc'=>isset($q->childNodes[2]->childNodes[0]->textContent) ? $this->clear( $q->childNodes[2]->childNodes[0]->textContent ) : ''
          ];
          array_push($position, $tmp);
        }
      }
      if( !empty($position) ) $res->position = $position;

      # summary
      foreach(pq('div.description p') as $q){
        $res->about = $this->clear( $q->nodeValue );
      }

      # publications
      $publications = [];
      foreach(pq('li.publication') as $q){
        $tmp = [];
        $tmp['title'] = $this->clear( $q->childNodes[0]->childNodes[0]->textContent );
        $tmp['where'] = $this->clear( $q->childNodes[0]->childNodes[1]->textContent);
        $tmp['text'] = $this->clear( $q->childNodes[1]->textContent );
        $tmp['authors'] = $this->clear( $q->childNodes[2]->textContent );
        array_push($publications, $tmp);
      }
      if( !empty($publications) ) $res->publications = $publications;

      # projects
      $projects = [];
      foreach(pq('li.project') as $q){
        if(isset($q->childNodes[0]->childNodes[0]->childNodes[0]->textContent)){
          $tmp = [
            'date'=> $q->childNodes[1]->childNodes[0]->textContent,
            'title'=> $this->clear( $q->childNodes[0]->childNodes[0]->childNodes[0]->textContent ),
            'desc'=>$this->clear( $q->childNodes[2]->textContent )
          ];
          array_push($projects, $tmp);
        }
      }
      if( !empty($projects) ) $res->projects = $projects;

      # interests
      $interest = [];
      foreach(pq('li.interest') as $q){
        if(trim($q->textContent)!='See less')
          array_push($interest, $this->clear( trim($q->textContent)) );
      }
      if( !empty($interest) ) $res->interest = implode(", ",$interest);

      # languages
      $language = [];
      foreach(pq('li.language > div') as $q){
        array_push($language, [
          'name'  => $q->childNodes[0]->textContent,
          'level' => $q->childNodes[1]->textContent
        ]);
      }
      if( !empty($language) ) $res->language = $language;

      return $res;

    } catch(Error $e){}

  }

}
