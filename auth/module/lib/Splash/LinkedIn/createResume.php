<?php

include_once __DIR__.'/../../../../../system/crud.php';

class CreateResume extends crud {

  public function __construct($db = ['db_pref'=>'ru','prevent'=>'ru']){
    parent::__construct($db);
  }

  /**
  * create CV if not exists
  */
  public function create(){
    # get user data
    if( isset($_POST['data']) && isset($_POST['data']['link']) ){
      $q = $this->getData('registerusers',['id','country'],['linkedin'=>$_POST['data']['link']],[],1);

      if(isset($q[0]['id'])){

        $data = (object)$_POST['data'];
        $user_id = $q[0]['id'];

        # insert to DB CV props
        $cv = [
          'user_id'=>$q[0]['id'],
          'expirience'=>rand(1,3),
          'city'=>'',
          'country'=>$q[0]['country'],
          'expects'=>'',
          'type'=>1,
          'date'=>date('Y-m-d H:i:s'),
          'position'=>'',
          'views'=>1
        ];

        # salary
        $salary = rand(8,15);
        $salary = $salary>10 ? $salary*100 : $salary+1000;
        $cv['salary']=$salary;

        try {

          # export user skills
          if(isset($data->skills)){
            for($i=0, $c=count($data->skills); $i<$c;$i++){
              $data->skills[$i] = $this->clear( $data->skills[$i] );
              # get skills id from table
              $id = $this->getData('special_catlist',['id'],[ "name"=>$data->skills[$i] ],[],1);
              if(isset($id[0]['id'])){
                $id = $id[0]['id'];
              } else {
                $id = $this->insertData('special_catlist',['name'=>$data->skills[$i]]);
              }
              # create user skill
              $this->insertData('user_skills',['user_id'=>$user_id,'skill_id'=>$id]);
            }
          }

          # interests
          if(isset($data->interest))
            $this->updateData('registerusers',['id'=>$user_id],['interestings'=>$data->interest],[],1);

          #create work
          $work = "";
          if(isset($data->work) && count($data->work)>0){
            for($i=0,$c=count($data->work); $i<$c;$i++){
              $work .= $data->work[$i]['name']."\n".$data->work[$i]['desc']."\n".$data->work[$i]['date']."\n\n".$data->work[$i]['text']."\n\n";
            }
          }
          $cv['work'] = $work;
          
            # work
            // $work = "";
            // if(isset($data->textWork)){
            //   $work = $this->clear( $data->textWork );
            // }
            // $cv['work'] = htmlspecialchars( mysql_real_escape_string($work));

          # education
          $edu = [];
          for($i=0,$c=count($data->edu); $i<$c;$i++){
            $edu[$i] = new stdClass;
            $edu[$i]->name = $this->clear($data->edu[$i]['name']);
            $edu[$i]->start = $data->edu[$i]['date'][0];
            $edu[$i]->complete = $data->edu[$i]['date'][1];
          }
          $edu = $this->json_encode_cyr($edu);
          $cv['education'] = $edu;

          # language
          $lang = [];
          for($i=0,$c=count($data->lang); $i<$c;$i++){
            $lang[$i] = new stdClass;
            $lang[$i]->name = $this->clear( $data->lang[$i]['name'] );
            $lang[$i]->type = $data->lang[$i]['type'];
          }
          $lang = $this->json_encode_cyr($lang);
          $cv['lang'] = $lang;

          $id = $this->insertData('resume',$cv);

        } catch(Error $e){
          file_put_contents(__DIR__.'/create.log',$_POST['data']['link']."\n",FILE_APPEND);
        }
      }
    }
    // clear data
    $list = json_decode( file_get_contents( __DIR__.'/task.json' ) );
    foreach($list as $k=>$v){
      if($v==$_POST['data']['link']){
        unset($list[$k]);
      }
    }
    file_put_contents( __DIR__.'/task.json', json_encode($list) );

    echo '{"response":1}';
  }

  public function json_encode_cyr($inObj) {
    $trans = array(
      '\u0430'=>'а', '\u0431'=>'б', '\u0432'=>'в', '\u0433'=>'г',
      '\u0434'=>'д', '\u0435'=>'е', '\u0451'=>'ё', '\u0436'=>'ж',
      '\u0437'=>'з', '\u0438'=>'и', '\u0439'=>'й', '\u043a'=>'к',
      '\u043b'=>'л', '\u043c'=>'м', '\u043d'=>'н', '\u043e'=>'о',
      '\u043f'=>'п', '\u0440'=>'р', '\u0441'=>'с', '\u0442'=>'т',
      '\u0443'=>'у', '\u0444'=>'ф', '\u0445'=>'х', '\u0446'=>'ц',
      '\u0447'=>'ч', '\u0448'=>'ш', '\u0449'=>'щ', '\u044a'=>'ъ',
      '\u044b'=>'ы', '\u044c'=>'ь', '\u044d'=>'э', '\u044e'=>'ю',
      '\u044f'=>'я',
      '\u0410'=>'А', '\u0411'=>'Б', '\u0412'=>'В', '\u0413'=>'Г',
      '\u0414'=>'Д', '\u0415'=>'Е', '\u0401'=>'Ё', '\u0416'=>'Ж',
      '\u0417'=>'З', '\u0418'=>'И', '\u0419'=>'Й', '\u041a'=>'К',
      '\u041b'=>'Л', '\u041c'=>'М', '\u041d'=>'Н', '\u041e'=>'О',
      '\u041f'=>'П', '\u0420'=>'Р', '\u0421'=>'С', '\u0422'=>'Т',
      '\u0423'=>'У', '\u0424'=>'Ф', '\u0425'=>'Х', '\u0426'=>'Ц',
      '\u0427'=>'Ч', '\u0428'=>'Ш', '\u0429'=>'Щ', '\u042a'=>'Ъ',
      '\u042b'=>'Ы', '\u042c'=>'Ь', '\u042d'=>'Э', '\u042e'=>'Ю',
      '\u042f'=>'Я',
      '\u0456'=>'і', '\u0406'=>'І', '\u0454'=>'є', '\u0404'=>'Є',
      '\u0457'=>'ї', '\u0407'=>'Ї', '\u0491'=>'ґ', '\u0490'=>'Ґ'
    );
    return strtr( json_encode($inObj), $trans);
  }

  public function clear($a){
    return mysql_real_escape_string( str_replace(['"','`',"'"],'',$a) );
  }

}

$CreateResume = new CreateResume;

if(isset($_POST['f59i_d844kg_xt']) && $_POST['f59i_d844kg_xt'] == md5(sha1('n%54i95xtsfisdkfsdfcdk548j3cdsdfdf'))){
  $CreateResume->create();
}
