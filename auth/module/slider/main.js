var Slider = (function(){
  var set = {
    prop:{
      // animate duration values
      time:10000,
      wait:500,
      cont:300,
      active:'init'
    },
    list:{
      init:{
        show:false
      },
      comp:{
        show:false
      },
      game:{
        show:false
      }
    },
    names:['init','comp','game']
  }
  // next slide name
  set.getNext = function(a){
    return ((set.names.indexOf(a)+1)==set.names.length)?'init':set.names[set.names.indexOf(a)+1]
  }

  // show basic slide cont
  set.showSlide = function(a,b){
    $('.sd-'+b).css({'display':'block','opacity':1.0})
    $('.sd-'+a).animate({'opacity':0.0},250,'easeOutCirc',function(){
      $(this).css({'display':'none','z-index':0}).children('*').css({'display':'none','opacity':0})
      set.prop.active = b
      if(set.list[b].show){
        $('.sd-'+b).delay(set.prop.wait).css('z-index',1).children('*').css('display','block').animate({'opacity':1.0},set.prop.cont,'easeOutCirc')
        window.setTimeout(function(){
          set.showSlide(b,set.getNext(b))
        },set.prop.time)
      } else {
        set[set.prop.active]()
      }
    })
  }

  set.init = function(){
    $('.sd-init').delay(set.prop.wait).css({'z-index':1}).children('*').css('display','block').animate({'opacity':1.0},set.prop.cont,'easeOutCirc')
    set.list.init.show = 1
    window.setTimeout(function(){
      set.showSlide('init','comp')
    },set.prop.time)
  }

  set.comp = function(){
    $('.sd-comp').children('.sd-complogo').delay(set.prop.wait).css('display','block').animate({'opacity':1.0},set.prop.cont,'easeOutCirc')
    $('.sd-comp').children('.comp-man').delay(set.prop.wait*1.5).css('display','block').animate({'opacity':1.0,'left':'48%'},set.prop.cont*1.5,'easeOutCirc')
    $('.sd-comp').delay(set.prop.wait).css({'z-index':1}).children('.sd-motto,.sd-caption,.sd-btn').css('display','block').animate({'opacity':1.0},set.prop.cont,'easeOutCirc')
    set.list.comp.show = 1
    window.setTimeout(function(){
      set.showSlide('comp','game')
    },set.prop.time)
  }

  set.game = function(){
    $('.sd-game').children('.sd-lgame').delay(set.prop.wait*1.5).css('display','block')
      .animate({'opacity':1.0,'top':'20%'},set.prop.cont,'easeOutCirc',function(){
        $(this).animate({'top':'23%'},set.prop.cont*2.0,'easeInCirc',function(){
          $(this).animate({'top':'20%'},set.prop.cont*2.3,'easeOutCirc')
        })
      })

    $('.sd-game').delay(set.prop.wait).css({'z-index':1}).children('.sd-motto,.sd-caption,.sd-btn').css('display','block').animate({'opacity':1.0},set.prop.cont,'easeOutCirc')
    set.list.game.show = 1
    window.setTimeout(function(){
      set.showSlide('game','init')
    },set.prop.time)
  }

  set.start = function(){
    set.init()
  }
  set.start()

  return set
})()
