<? if(isset($_GET['i']) && (int)$_GET['i']>0){
    $i=(int)$_GET['i']>0?(int)$_GET['i']:1;
    include_once('../system/init.php');
    $data = $core->unwrap($core->getData('presentation','*',array('id'=>$i),array(),1));
} else { header('Location: index'); die();} 
?>
<!DOCTYPE html>
<html lang="<?=$loc->lang?>">
<head>
    <meta charset="utf-8"/>
    <title><?=$data['name']?></title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="css/show.css" />
    <? include("../block/options.php"); ?>
<body>
    <div class="white-zone col-lg-12" id="title-box">
        <div class="elem-logo col-lg-10">
            <span class="prev-text"><?=$data['name']?>             
                <div id="logo-ico">
                    <i class="<?=$data['ico']?>"></i>
                </div>
                <span id="come-back"><a href="index"><i class="icon-reply"></i> <?=$loc->data->i[0]?></a></span>
            </span>
        </div>
        <div class="title-box"><?=$data['motto']?></div>
    </div>
    
    <?=$data['text']?>
        
    <div id="footer-box">
        <img src="../images/slider/logo.png" alt="" />
        <div class="line-logo"></div>
    </div>
                
    <script type="text/javascript" src="../css/app.v2.js"></script>
    <script type="text/javascript" src="script/show.js"></script>
</body>
</html>