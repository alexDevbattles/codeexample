<?php
header("Cache-Control: no-store,no-cache,must-revalidate");
header("Expires: ".date("r"));

  if(!isset($core)) include_once '../system/crud.php';
  include_once '../system/helper/user.php';
  include_once '../system/helper/help.php';

  class icore extends crud {

    public $d = array();
    public $user;
    public $help;
    public $list = array();
    public $slot = array(array('Sand','Tour','Discus','StatSand'),array('StatDiscus','Discus','Tour','Sand'),array('StatSand','Discus','Tour','Sand'),array('Sand','Tour','StatSand','Discus'),array('Tour','Sand','Discus','StatSand'));

    public function __construct(){
        parent::__construct(array('db_pref'=>$this->db_pref,'connectAll'=>1));
        $this->d = $this->parseType('POST');
        $this->user = new User(array('db'=>$this->db,'db_pref'=>'ru','prevent'=>'ru'));
if($this->isAjax()) $this->parseAjax();
    }

    public function render() {
        $data = array();
        $data['Tour']=&$this->list['tour'];
        $data['Sand']=&$this->list['cont']['sand'];
        $data['Discus']=&$this->list['cont']['discus'];
        $data['StatSand']=&$this->list['stat']['sand'];
        $data['StatDiscus']=&$this->list['stat']['discus'];

        $p = 'render';
        $iter = array('StatSand'=>0,'Discus'=>0,'Tour'=>0,'StatDiscus'=>0,'Sand'=>0);
        for($i=0,$c=count($this->slot);$i<$c;$i++){
            if($this->randomize()) $this->slot[$i] = array_reverse($this->slot[$i]);
            for($j=0,$y=count($this->slot[$i]);$j<$y;$j++){
                $this->slot[$i][$j]($data[$this->slot[$i][$j]][$iter[$this->slot[$i][$j]]]);
                $iter[$this->slot[$i][$j]]+=1;
            }
        }
    }

    public function getList($n=0){
        $this->help = new help();
    	$this->lim = ($n*5).',5';
        $this->makeStat();
        $this->makeCont();
        $this->makeTour();

        // easing for localization render
        global $loc;
        $this->post = &$loc->data->option->i[0];
        $this->discus = &$loc->data->option->i[1];
        $this->answer = &$loc->data->option->i[2];
        $this->anew = &$loc->data->option->i[3];
        $this->view = &$loc->data->option->i[4];
    }

    public function makeCont(){
        $this->list['cont']=array();
        $p = array('discus','sand');
        for($i=0,$c=count($p);$i<$c;$i++){
            $this->list['cont'][$p[$i]]=$this->getData($p[$i].'_post',array('id','title','view','date','comm','user_id'),array(),array('date'=>'desc'),$this->lim);
            if(!empty($this->list['cont'][$p[$i]])){
                if($this->randomize()) $this->list['cont'][$p[$i]] = array_reverse($this->list['cont'][$p[$i]]);
                $l=&$this->list['cont'][$p[$i]];
                for($j=0,$y=count($l);$j<$y;$j++){
                  $l[$j]['user']=$this->user->getUserView($l[$j]['author']); //($this->getData('registerusers',array('concat(name," ",surname) as name','img'),array('id'=>),array(),1));
                  $l[$j]['date']=$this->help->getPubDate($l[$j]['date']);
                  $l[$j]['cat']=$this->getData($p[$i].'_cat',array('cat'),array('post_id'=>$l[$j]['id']),array(),1);
                  $l[$j]['name']=$this->unwrap($this->getData($p[$i].'_catlist',array('name'),array('id'=>$l[$j]['cat'][0]['cat']),array(),1));
                  if($p[$i]=='discus'){
                    $l[$j]['comm']=$this->getData('discus_comm',array('count(id) as c'),array('post_id'=>$l[$j]['id']),array(),15);
                    $l[$j]['comm']=$l[$j]['comm'][0]['c'];
                  }
                  if($p[$i]=='sand'){
                    $l[$j]['link'] = $this->getData('sand_post',['link'],['id'=>$l[$j]['id']],[],1);
                    $l[$j]['link'] = $l[$j]['link'][0]['link'];
                  }
                }
            }
        }
    }

    public function makeTour(){
        $this->db_pref='ru';
        $this->list['tour']=$this->getData('test_archive',array('coins as ball','win_id','cat','live_date'),array('win_id!'=>0),array('live_date'=>'desc'),$this->lim);
        if(!empty($this->list['tour'])){
            $l=&$this->list['tour'];
            for($i=0,$c=count($l);$i<$c;$i++){
                $l[$i]['user']=$this->user->getUserView($l[$i]['win_id']); // $this->unwrap($this->getData('registerusers',array('concat(name," ",surname) as name','img'),array('id'=>$l[$i]['win_id']),array(),1));
                $l[$i]['name']=$this->unwrap($this->getData('cat',array('name'),array('id'=>$l[$i]['cat']),array(),1));
                $l[$i]['live_date']=$this->help->getPubDate($l[$i]['live_date']);
            }
            if($this->randomize()) $this->list['tour'] = array_reverse($this->list['tour']);
        }
        $this->db_pref=$this->lang;
    }

    public function makeStat(){
        $this->list['stat']=array();
        $p = array('discus','sand');
        for($i=0,$c=count($p);$i<$c;$i++){
            $this->list['stat'][$p[$i]]=$this->toArray(mysql_query("SELECT cat,count(id) as c FROM `{$p[$i]}_cat` group by cat order by c desc limit {$this->lim}",$this->db[$this->db_pref]));
            if($this->randomize()) $this->list['stat'][$p[$i]] = array_reverse($this->list['stat'][$p[$i]]);
            if(!empty($this->list['stat'][$p[$i]])){
                $l=&$this->list['stat'][$p[$i]];
                for($j=0,$y=count($l);$j<$y;$j++){
                  $l[$j]['name']=$this->unwrap($this->getData($p[$i].'_catlist',array('name'),array('id'=>$l[$j]['cat']),array(),1));
                  if(strlen($l[$j]['name']['name'])>6) $l[$j]['sm']=1;
                }
            }
        }
    }

    public function randomize(){
        $a = mt_rand(1,10);
        if($a%2==0) return true;
    }

    public function parseAjax(){
        $ajax_conf = array(
           //'more'=>array('i')
        );
        if($this->d['get'] && $ajax_conf[$this->d['get']] && $this->inPost($ajax_conf[$this->d['get']])){
            $this->{$this->d['get']}();
        }
    }
  }
  $icore = new icore();
  if(isset($core)) $icore->getList();
?>
