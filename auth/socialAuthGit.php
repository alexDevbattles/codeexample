<?
	$client_id = 'f833cceff24f948dcb12';
    $redirect_url = 'http://devbattles.com/auth/socialAuthGit.php';
    $secret_key = 'e1dd80bf75cae1f091066720408e529b67ce5d62';

include_once("authClass.php");
 //get request , either code from github, or login request
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        //authorised at github
                if(isset($_GET['code']))
        {
            $code = $_GET['code'];

            //perform post request now
            $post = http_build_query(array(
                'client_id' => $client_id ,
                'redirect_uri' => $redirect_url ,
                'client_secret' => $secret_key,
                'code' => $code ,
            ));

            $params = stream_context_create(array("http" => array(
                "method" => "POST",
                "header" => "Content-Type: application/x-www-form-urlencodedrn" .
                            "Content-Length: ". strlen($post) . "rn".
                            "Accept: application/json" ,
                "content" => $post,
            )));

            $url = 'https://github.com/login/oauth/access_token';

		    $curl = curl_init();
		    curl_setopt($curl, CURLOPT_URL, $url);
		    curl_setopt($curl, CURLOPT_POST, 1);
		    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		    $result = curl_exec($curl);
		    curl_close($curl);
		    parse_str($result, $result);

            $access_token = $result['access_token'];

            $url = "https://api.github.com/user?access_token=$access_token";

			$curl = curl_init();
			curl_setopt_array($curl, array(
			    CURLOPT_RETURNTRANSFER => 1,
			    CURLOPT_URL => $url,
			    CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT']
			));
			$result = curl_exec($curl);
			curl_close($curl);
			$user_data  = json_decode($result , true);

            if (is_array($user_data)){
		        $auth->data['name'] =  (isset($user_data['name']))?$user_data['name']:false;
		        $auth->data['img'] = $user_data['avatar_url'];
		        $auth->data['date'] = date("Y-m-d");
				$auth->data['site'] =  (isset($user_data['blog']))?$user_data['blog']:false;
				$auth->data['email'] =  (isset($user_data['email']))?$user_data['email']:false;

		        $auth->data['github'] = $user_data['login'];
		        $auth->socAuth('github',$user_data['login']);
		    }

        }
        else
        {
            $url = "https://github.com/login/oauth/authorize?client_id=$client_id&redirect_uri=$redirect_url&scope=user";
            header("Location: $url");
        }
    }
