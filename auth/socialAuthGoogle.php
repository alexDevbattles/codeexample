<?php

$client_id = '1029518695565-5hcgbmbnpq8n15admtis0uj3sr27l5b8.apps.googleusercontent.com'; // Client ID
$client_secret = 'rgEbiD97s2nOp1rI39eWSYdZ'; // Client secret
$redirect_uri = 'http://www.devbattles.com/auth/socialAuthGoogle.php'; // Redirect URIs

$url = 'https://accounts.google.com/o/oauth2/auth';

$params = array(
    'redirect_uri'  => $redirect_uri,
    'response_type' => 'code',
    'client_id'     => $client_id,
    'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
);

//echo $link = '<p><a href="' . $url . '?' . urldecode(http_build_query($params)) . '">Аутентификация через Google</a></p>';
header('Refresh: 0; URL="'.$url.'?'.urldecode(http_build_query($params)).'"');

include_once("authClass.php");

if (isset($_GET['code'])) {
    $result = false;

    $params = array(
        'client_id'     => $client_id,
        'client_secret' => $client_secret,
        'redirect_uri'  => $redirect_uri,
        'grant_type'    => 'authorization_code',
        'code'          => $_GET['code']
    );

    $url = 'https://accounts.google.com/o/oauth2/token';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
    curl_close($curl);
    $tokenInfo = json_decode($result, true);

    if (isset($tokenInfo['access_token'])) {
        $params['access_token'] = $tokenInfo['access_token'];

        $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
        if (isset($userInfo['id'])) {
            $userInfo = $userInfo;
            $result = true;
        }
    }

    if ($result) {
        $auth->data['name'] =  $userInfo['name'].' '.$userInfo_vk['surname'];
        $auth->data['email'] = $userInfo['email'];
        $auth->data['img'] = $userInfo['picture'];
        $auth->data['date'] = date("Y-m-d");
        $auth->data['google'] = $userInfo['id'];
        $auth->socAuth('google',$userInfo['id']);
    }
}

function toArray($query){
    $res = array();
    while($row = mysql_fetch_assoc($query)){
        $res[] = $row;
    }
    return $res;
}

?>

