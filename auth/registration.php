<? include_once '../system/init.php';
   include_once 'authClass.php';
if(!isset($_GET['i'])) $_GET['i']='user';
if((int)$_GET['i']>0){
  $u = $auth->getUser((int)$_GET['i']);
  $title = $loc->data->res->i[0];
}
?>
<!DOCTYPE html>
<html lang="<?=$loc->lang?>">
<head>
    <meta charset="utf-8"/>
    <title><?=$title?></title>
    <meta name="description" content="<?=$loc->data->res->i[1]?>"/>
    <meta name="viewport" content="width=device-width">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
    <?php include("../js/analitics.php"); ?>
    <link rel="stylesheet" href="css/nav.css"/>
    <link rel="stylesheet" href="css/authForm.css"/>
    <? if(isset($u)){ ?>
    <meta property="og:image" content="http://www.devbattles.com/images/dev-logo.jpg" />
    <meta property="og:title" content="<?=$title?>" />
    <meta property="og:description" content="<?=$loc->data->res->i[1]?>" />
    <meta property="og:url" content="http://www.devbattles.com/en/auth/registration-<?=$_GET['i']?>" />
    <meta property="og:site_name" content="IT Social Network DevBattles"/>
    <? } else {?>
      <meta property="og:image" content="http://www.devbattles.com/images/dev-logo.jpg" />
    <? } ?>
    <? include("../block/options.php");?>
<body>
<section class="hbox stretch">
    <section id="content">
        <section class="vbox">
          <div id="slider-box">
            <? include_once 'head.inc.php'; ?>
          </div>
          <section class="scrollable wrapper">
                <div class="col-lg-12">

                    <? if($_GET['i']=='comp'):?>
                    <div class="col-md-4 comp-info">
                        <?=$core->set['page']->cont['content']?>
                    </div>
                    <? endif; ?>

                    <? if((int)$_GET['i']>0):
                       if($u){?>
                          <div class="col-lg-4 col-md-6 hidden-xs hidden-sm comp-info">
                            <div class="rsl-logo">
                              <img src="<?=$u['img']?>"/>
                            </div>
                            <span class="rsl-title"><span><?=$loc->data->res->i[2]?></span><br/><?=$loc->data->res->i[3]?></span>
                          </div>
                       <? } ?>
                    <? endif; ?>

                    <div class="col-lg-4 col-md-6 col-xs-12 <? if($_GET['i']=='comp' || is_numeric($_GET['i'])) {} else echo 'col-lg-offset-4  col-md-offset-3 '; ?>  main-box">
                        <section class="panel">
                            <header class="panel-heading bg bg-primary text-center"><?=$loc->data->head->i[2]?></header>
                            <form action="#" class="panel-body" autocomplete="on" name="registerform">
                                <div class="form-group"><label class="control-label"><?=$loc->data->i[2]?></label>
                                   <input type="text" placeholder="<?=$loc->data->i[3]?>" maxlength="30" class="form-control" id="name"  required="required"/>
                                </div>
                                <div class="form-group"><label class="control-label"><?=$loc->data->i[4]?></label>
                                    <input type="email" placeholder="zenyk@example.com" class="form-control" id="email"  required="required"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?=$loc->data->i[5]?> <span style="color: #aaa;"><?=$loc->data->i[6]?></span></label>
                                    <input type="password" id="password" placeholder="Password" maxlength="20" class="form-control" required="required"/>
                                </div>

                                <? if($_GET['i']!='comp'): ?>
                                <span class="auth-desc"><?=$loc->data->sign->i[3]?></span>
                                <div class="socio-box">
                                  <a class="socio-auth" href="socialAuthVk.php"><i class="i icon-vk"></i></a>
                                  <a class="socio-auth" href="socialAuthFacebook.php"><i class="i icon-facebook"></i></a>
                                  <a class="socio-auth" href="socialAuthGoogle.php"><i class="i icon-google-plus"></i></a>
                                  <a class="socio-auth" href="socialAuthGit.php"><i class="i icon-github"></i></a>
                                  <a class="socio-auth" href="socialAuthTw.php"><i class="i icon-twitter"></i></a>
                                  <a class="socio-auth" href="socialAuthLinkedIn.php"><i class="i icon-linkedin"></i></a>
                                </div>
                                <? endif; ?>

                                <div class="form-group">
                                    <label class="creaComp" style="display:none;"><input type="checkbox"<? if($_GET['i']=='comp') echo "checked='true'"; ?> id="creaComp" value="1"/></label>
                                </div>

                                <button type="submit" class="btn btn-success" id="go" ><?=$loc->data->i[0]?></button>
                                <div class="line line-dashed"></div>
                                <p class="text-muted text-center">
                                    <small class="if-is"><?=$loc->data->i[8]?></small>
                                </p>
                                <a href="account" class="btn btn-success btn-block"><?=$loc->data->i[9]?></a>
                            </form>
                        </section>
                    </div>

                </div>
            </section>
        </section>
    </section>
</section>

<script src="../css/app.v2.js"></script>
<? $core->toJs($loc->data->js,'ini_loc'); ?>
<script type="text/javascript"> var dir_of_red = '<?=$_GET['i']?>';</script>
<? if(isset($_GET['b'])) $core->toJs($_GET['b'],'ini_act'); ?>
<script src="script/registration.js"></script>
</body>
</html>
