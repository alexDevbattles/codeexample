<? if(isset($_GET['i']) && (int)$_GET['i']>0){
    $i=(int)$_GET['i']>0?(int)$_GET['i']:1;
    include_once('../system/init.php');
    $data = $core->unwrap($core->getData('info','*',array('id'=>$i),array(),1));
} else { header('Location: index'); die();} 
?>
<!DOCTYPE html>
<html lang="<?=$loc->lang?>">
<head>
    <meta charset="utf-8"/>
    <title><?=$data['name']?></title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" type="text/css" href="css/show.css" />
    <link rel="stylesheet" type="text/css" href="css/info.css" />
    <? include("../block/options.php"); ?>
<body>
    <div class="white-zone col-lg-12 title-box">
        <div class="elem-logo col-lg-10">
            <span class="prev-text"><?=$data['name']?>             
                <span id="come-back"><a href="index"><i class="icon-reply"></i> <?=$loc->data->i[0]?></a></span>
            </span>
        </div>
        <div class="top-nav col-lg-10">
            <a href="info-1-<?=$core->url($loc->data->i[1]);?>"><?=$loc->data->i[1]?></a>
            <a href="info-2-<?=$core->url($loc->data->i[2])?>"><?=$loc->data->i[2]?></a>
        </div>
        
    </div>
    
    <div class="white-zone col-lg-12">
        <div class="col-lg-1" style="padding: 0 !important;"></div>
        <div class="col-lg-7">
        
            <?=$data['text']?>
        </div>
    </div>
        
        
    <div id="footer-box">
        <img src="../images/slider/logo.png" alt="" />
        <div class="line-logo"></div>
    </div>
                
    <script type="text/javascript" src="../css/app.v2.js"></script>
    <script type="text/javascript">
        $(function(){

            $('.top-nav').children('a').eq(parseInt('<?=$_GET['i']?>')-1).addClass('act-nav');
        });
    </script>
</body>
</html>