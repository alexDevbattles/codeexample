<?php
session_start();
$base_dir = defined('FACEBOOK_SDK_V4_SRC_DIR') ? FACEBOOK_SDK_V4_SRC_DIR : __DIR__ . '../system/helper/manager/social/facebook/vendor/facebook/';
require('../system/helper/manager/social/facebook/vendor/autoload.php');

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

// init app with app id and secret
FacebookSession::setDefaultApplication( '189217001288226','67642712757e445d16e6d7b4e591575a' );

// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper('http://devbattles.com/auth/socialAuthFacebook.php');

try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ){
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}

// see if we have a session
if(isset($session)){

  // Exchange the short-lived token for a long-lived token.
  $accessToken = $session->getAccessToken();
  $longLivedAccessToken = $accessToken->extend();
  $longLivedAccessToken = $longLivedAccessToken->__toString();

  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();

  // make system auth
  include_once 'authClass.php';
  $auth->data['name'] =  ($graphObject->getProperty('first_name') ? $graphObject->getProperty('first_name') : '' ).' '.($graphObject->getProperty('last_name') ? $graphObject->getProperty('last_name') : '' );
  $auth->data['email'] = $graphObject->getProperty('email') ? $graphObject->getProperty('email') : '';
  $auth->data['img'] = 'http://graph.facebook.com/'.$graphObject->getProperty('id').'/picture?type=large';
  $auth->data['date'] = date("Y-m-d");
  $auth->data['facebook'] = $graphObject->getProperty('id');
  $auth->token = [
    'type'=>'facebook',
    'value'=>$longLivedAccessToken
  ];
  $auth->socAuth('facebook',$graphObject->getProperty('id'));
} else {
  // redirect to login
  header("Location: ".$helper->getLoginUrl(['email', 'public_profile','publish_actions']));
}

?>
