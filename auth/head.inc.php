<header class="header main-header trans-100">
   <div id="main-logo" class="trans-150-all">
      <img src="../images/slides/devlogo/full-150w.png"/>
      <img src="../images/slides/devlogo/circ-70w.png" style="display:none; opacity:0.0;" alt="DevBattles" title="DevBattles"/>
   </div>
   <div id="top-nav" class="hb-panel trans-100-all">
      <a href="../sand/index"><?=$loc->nav->sand?></a>
      <a href="../discussion/index"><?=$loc->nav->diss?></a>
      <a href="../company/index"><?=$loc->nav->comp?></a>
      <a href="../company/vacant"><?=$loc->nav->vacant?></a>
   </div>
   <!--div class="reed-us">
       <span id="reed-in" class="trans-100-all"><span><?=$loc->data->sub->i[0]?></span></span>
       <div id="reed-in-form" class="index-form <?if($loc->lang=='en') echo 'en-sub';?>">
           <div class="top-conus rotate-45"></div>
           <div class="form-cont">
               <span class="auth-desc center-desc"><?=$loc->data->sub->i[1]?></span>
               <div class="socio-box trans-50-all">
                   <a href="https://www.linkedin.com/groups/DevBattles-6712713?home=&gid=6712713"><i class="icon-linkedin"></i> LinkedIn</a>
                   <a href="https://www.facebook.com/groups/627132700693909/"><i class="icon-facebook"></i> Facebook</a>
                   <a href="http://www.vk.com/devbattles"><i class="icon-vk"></i> Vkontakte</a>
                   <a href="https://plus.google.com/u/0/103880818294878830309/posts"><i class="icon-google-plus"></i> Google Plus</a>
                   <a href="https://www.youtube.com/channel/UChxld6dsAwpzafyKNbQRsIA"><i class="icon-youtube"></i> YouTube</a>
               </div>
               <div class="b-line"></div>
               <img src="../images/icon-close.png" class="auth-close trans-100-all"/>
           </div>
       </div>
   </div-->
   <div id="auth-panel" class="b-b b-r <?if($loc->lang=='en') echo 'en-panel';?>">
       <div id="log-in" class="trans-100-all"><?=$loc->data->head->i[0]?></div>
       <div id="log-in-form" class="index-form <?=$core->lang?>-auth">
           <div class="top-conus rotate-45"></div>
           <form class="form-cont login-form">
               <input type="email" name="email" id="h-email" placeholder="<?=$loc->data->log->i[0]?>" value="" required/>
               <input type="password" name="password" id="h-password" placeholder="<?=$loc->data->log->i[1]?>" value="" required/>
               <input type="submit" class="btn btn-success" id="login-send" value="<?=$loc->data->log->i[2]?>"/>
               <a href="account-forget" class="auth-forget"><?=$loc->data->log->i[3]?></a>
               <span class="auth-desc"><?=$loc->data->log->i[4]?></span>
               <div class="socio-box">
                   <a class="socio-auth" href="socialAuthVk.php"><i class="i icon-vk"></i></a>
                   <a class="socio-auth" href="socialAuthFacebook.php"><i class="i icon-facebook"></i></a>
                   <a class="socio-auth" href="socialAuthGoogle.php"><i class="i icon-google-plus"></i></a>
                   <a class="socio-auth" href="socialAuthGit.php"><i class="i icon-github"></i></a>
                   <a class="socio-auth" href="socialAuthTw.php"><i class="i icon-twitter"></i></a>
                   <a class="socio-auth" href="socialAuthLinkedIn.php"><i class="i icon-linkedin"></i></a>
               </div>
               <div class="b-line"></div>
               <img src="../images/icon-close.png" class="auth-close trans-100-all"/>
           </form>
       </div>
       <span class="or-desc b-b"><?=$loc->data->head->i[1]?></span>
       <div id="sign-in" class="b-b"><?=$loc->data->head->i[2]?></div>
       <div id="sign-in-form" class="index-form <?=$core->lang?>-auth">
           <div class="top-conus rotate-45"></div>
           <div class="form-cont">
               <span class="auth-desc center-desc sign-desc"><?=$loc->data->sign->i[0]?></span>
               <a class="btn btn-white new-user" href="registration"><i class="icon-group"></i> <?=$loc->data->sign->i[1]?></a>
               <a class="btn btn-white new-comp" href="registration-comp"><i class="icon-briefcase"></i> <?=$loc->data->sign->i[2]?></a>
               <span class="auth-desc"><?=$loc->data->sign->i[3]?></span>
               <div class="socio-box">
                   <a class="socio-auth" href="socialAuthVk.php"><i class="i icon-vk"></i></a>
                   <a class="socio-auth" href="socialAuthFacebook.php"><i class="i icon-facebook"></i></a>
                   <a class="socio-auth" href="socialAuthGoogle.php"><i class="i icon-google-plus"></i></a>
                   <a class="socio-auth" href="socialAuthGit.php"><i class="i icon-github"></i></a>
                   <a class="socio-auth" href="socialAuthTw.php"><i class="i icon-twitter"></i></a>
                   <a class="socio-auth" href="socialAuthLinkedIn.php"><i class="i icon-linkedin"></i></a>
               </div>
               <div class="b-line"></div>
               <img src="../images/icon-close.png" class="auth-close trans-100-all"/>
           </div>
       </div>
   </div>
   <div id="lang-panel" class="b-b b-r">
        <a class="b-b <?if($loc->lang=='en') echo 'active';?>" href="http://<?=$core->site_name.'/en'.substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-4)?>">En</a>
        <a class="b-b <?if($loc->lang=='ru') echo 'active';?>" href="http://<?=$core->site_name.'/ru'.substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-4)?>">Ru</a>
   </div>
</header>
