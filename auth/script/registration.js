$(document).ready(function(){

    $.getScript('script/head.js');

    $('.navbar-collapse:first').children('form').remove()
    $('.rsl-logo').css('height',$('.rsl-logo').width())

    var dir_type = {
	  'user':'../user/index',
	  'game':'../tournaments/index',
	  'comp':'../company/add',
	  'discuss':'../user/index'
    }

    //if(typeof ini_act!=='undefined') dir_type.game='../user/index'

    $("form[name='registerform']").submit(function(e){
        e.preventDefault()
        var name = $("#name").val()
        var email = $("#email").val()
        var password = $("#password").val()

        var err = true;
        $(".panel-body .form-group input").each(function(){
            if($.trim($(this).val()).length==0){
                $(this).addClass("border")
                err = false;
            } else {
                $(this).removeClass("border")
            }
        });

        if(!isPass(password)){ $("#password").addClass('border');}
        if(!isEmail(email)){ $("#email").addClass('border');}

        if(err && isEmail(email) && isPass(password)){

            $.ajax({
                url: "authClass.php",
                type: "POST",
                data: ({get:'add',name:name, email:email, password:password, dir:dir_of_red}),
                dataType: "html",
                async:false,
                success:function(e){
                  if(e){
                    if(e=="E"){
                        $("#email").addClass('border');
                        alert('email already occupied');
                    } else if(parseInt(e)>0){

                        Main.req({
                          url:'../system/services/login/set.php',
                          data:{state: 'main', page:location.href, type: 'input'},
                          success: function(d){
                            if(typeof dir_of_red!=='undefined' && parseInt(dir_of_red)>0) dir_of_red = 'user'
                            if(typeof dir_of_red=='undefined') dir_of_red='user';
                            window.location.replace(dir_type[dir_of_red]);
                          }
                        })

                    } else if(e=='pmx'){alert(ini_loc.i[1])
                    } else if(e.split(' ')[0]=='challenge' && parseInt(e.split(' ')[1])>0){

                      Main.req({
                        url:'../system/services/login/set.php',
                        data:{state: 'game', page:location.href, type: 'input'},
                        success: function(d){
                          window.location.replace('../g-'+e.split(' ')[1])
                        }
                      })

                    }

                  }
                }
            }).responseText
        }
    })

    function isEmail(email) {
      //var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,12})+$/;
      //return regex.test(email);
      return email;
    }

    function isPass(pass) {
      /*var regex = /^([a-zA-Z0-9]{8,20})+$/;
      return regex.test(pass);*/
      return pass;
    }
});
