$(function(){

    /* Design Part */
    $('.stat-item').click(function(){
      window.location = $(this).attr('a')
    })

    /* resize user`s avatars */
    $('.img-item').each(function(){ $(this).css('height',$(this).width()) })

    /* go top foooter button */
    $('#go-top').click(function(){
      $('body').animate({scrollTop:0},600,'easeOutCirc')
    })

})
