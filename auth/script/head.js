$('.scrollable').on("scroll",function(){
    if ($(this).scrollTop()>70) $(".main-header").addClass("head-new")
    else $(".main-header").removeClass("head-new")
})

/* Core Part */
$('#main-logo').click(function(){window.location = 'index'})

/* opening header forms */
$('#sign-in,#log-in,#reed-in').click(function(){
    var name = '#'+$(this).attr('id')+'-form'
    if(typeof $(name).attr('open')=='undefined'){
        $('.index-form').css({'display':'none','opacity':0.0,'left':-50}).removeAttr('open')
        $(name).attr('open',1).css({'display':'block','opacity':0.0,'left':-50}).animate({'opacity':1.0,'left':-20},200,'easeOutCirc')
    }
    else
        $(name).removeAttr('open').animate({'opacity':0.0,'left':-60},200,'easeOutCirc',function(){
            $(this).css({'display':'block','opacity':0.0})
        })
})

/* close all header forms */
$('.auth-close').click(function(){
    $(this).closest('.index-form').removeAttr('open').animate({'opacity':0.0,'left':-50},200,'easeOutCirc',function(){
        $(this).css({'display':'block','opacity':0.0})
    })
})

$('.login-form').submit(function(e) {
    e.preventDefault()
    var email = $("#h-email").val()
    var password = $("#h-password").val()
    if(email!=='' && password!==''){
      $.ajax({
        url: "authClass.php",
        type: "POST",
        data: ({get:'login',email:email,password:password}),
        dataType: "html",
        async:false,
        success:function(e){
          if(e=="yes") window.location.replace("../user/index")
          else window.location.replace('account')
        }
      }).responseText
    }
})

$('.socio-box > a').bind('click',function(e){
  var type = $(this).children('i').attr('class').split(' ')[1].split('-')[1]
  Main.req({
    url:'../system/services/login/set.php',
    data:{state: 'main', page:location.href, type: type},
    success: function(d){
      location.href = e.attr('href')
    }
  })
})
