$(function(){

    $.getScript('script/head.js');

    $("form[name='loginform']").submit(function(e) {
        e.preventDefault();
        var email = $("#email").val();
        var password = $("#password").val();

        if(email!=='' && password!=='' && isEmail(email)){
          $.ajax({
            url: "authClass.php",
            type: "POST",
            data: ({get:'login',email:email,password:password}),
            dataType: "html",
            async:false,
            success:function(e){
              if(e=="yes"){
                  window.location.replace("../user/index");
              } else if(e=='no') {
                  var l = self.location.toString().split('/'); l=l[l.length-1];
                  if(l!=='account') document.location = 'account';
                  $('#authForm').animate({'marginLeft':100,'opacity':0.2},150,function(){
                    $("#email,#password").val('').addClass('border');
                  }).animate({'marginLeft':0,'opacity':1.0},150);
              } else if(e.split(' ')[0]=='challenge' && parseInt(e.split(' ')[1])>0){
                window.location.replace('../g-'+e.split(' ')[1])
              }
            }
          }).responseText;
        }
    });

    // cancel of restore pass
    $('#cancel-recheck').click(function(){
        $('#emailRecheck').fadeOut(100,function(){$(this).css({'display':'none'})});
        $('#loginform').css('display','block').animate({'opacity':1.0},100);
    });

    // show restore form
    $('#passMemory').click(function(e){
      e.preventDefault();
      $('#loginform').fadeOut(100,function(){$(this).css({'display':'none'})});
      $('#emailRecheck').css('display','block').animate({'opacity':1.0},100);
    });

    // actions with $_get param.
    if(typeof ini_action!=='undefined'){
      switch(ini_action){
        case 'forget':
          $('#passMemory').trigger('click')
        break
      }
    }

    // send request (form submit)
    $('#emailRecheck').submit(function(e){
        e.preventDefault();
        if($('#recovery-value').val()!==''){
        var email = $("#recovery-value").val();
        $.ajax({
          url: "authClass.php",
          type: "POST",
          data: ({get:'forget', email:email}),
          dataType: "html",
          async:false,
          success:function(r){
            if(r!==false){
              $('#'+r).css('display','block').animate({'opacity':1.0},100);
              if(r=='uok'){
                $('#rechek-button,#recovery-value').fadeOut(100,function(){$(this).remove();});
                $('#main-recovery').text('')
                $('#uok').text(loc.i[1])
                $('.rec-info,#uns').css({'display':'none','opacity':0.0})
                $('#cancel-recheck').html('<i class="icon-reply"></i> '+loc.i[0]);
              }
            }
          }
        }).responseText;
      }
    });

    // close alert massage
    $('.icon-remove').click(function(){
        $(this).closest('.informer').fadeOut(100);
    });

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
});
