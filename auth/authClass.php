<?php
  if(!isset($core)) include_once '../system/crud.php';
  include_once '../system/helper/region.php';
  class auth extends crud {

    public $data = array();
    public $area;

    public function __construct(){
      
      parent::__construct(array('db_type'=>'ru','prevent'=>'ru'));
      // get country of user
      $this->area = new region(false,array('db_pref'=>'ru','db'=>$this->db,'prevent'=>'ru'));
      $this->area = $this->area->getCountry($this->area->detect());
      $this->data = $this->getPost();
      if($this->isAjax()) $this->parseAjax();
    }

    /**
     * auth using social services
    */
    public function socAuth($a,$b){

      $user_id = $this->checkUser($a,$b);
      if($user_id){
         $this->data = $this->unwrap($this->getData('registerusers',array('concat{name," ",surname} as name','email'),array('id'=>$user_id),array(),1));
         $this->detectData($user_id);
      } else {
        $user_id = $this->addUser($this->data,0);
        if($user_id){
          $this->insertData('user_auth',array('user_id'=>$user_id,'type'=>$a));
          $this->additionals( $user_id );
        }
      }

      // set item for social posting
      if(isset($this->token)){
        $this->saveToken($user_id);
        if($a == 'facebook'){
          $itemName = "auth_thanks_".$a;
          if( !isset($this->tokenLog) || isset($this->tokenLog) && !isset($this->tokenLog->{$itemName}) ){
            $id = $this->insertData('public_actions',[
              'user_id'=>$user_id,
              'type'=>$a,
              'item'=>'auth_thanks',
              'date'=>'Y-m-d H:i:s'
            ]);
            /* call public api */
            try {
              $ch = curl_init();
          		//set the url, number of POST vars, POST data
          		curl_setopt($ch,CURLOPT_URL, "http://www.devbattles.com/system/helper/manager/public_actions/cron.php");
          		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          		curl_exec($ch);
          		curl_close($ch);
            }
            catch(Error $e){}
          }
        }
      }

      // make redirects
      if($user_id){
        // if this is request form login widget
        if(isset($this->data['url'])) header('Location: '.$this->data['url']);
        // if this is invited from game user
        if($this->inSess('challenge')) header("Location: ../g-{$_SESSION['challenge']}");
        else
          $this->manageRedirects();
        exit;
      }
    }

    /* additional action after first login */
    public function additionals( $user_id ){
      # public resume
      if(isset($this->data['linkdn'])){
        if(strlen($this->data['linkedin'])>3){
          $task = json_decode( file_get_contents(__DIR__.'/module/lib/Splash/LinkedIn/task.json') );
          if(!in_array($this->data['linkedin'], $task)){
            array_push($task, $this->data['linkedin']);
            file_put_contents(__DIR__.'/module/lib/Splash/LinkedIn/task.json', json_encode($task));
            $_SESSION['task_resume'] = 1;
          }
          // require_once __DIR__.'/module/lib/Splash/LinkedIn/createCV.php';
          // $CreateCV = new CreateCV(['db'=>$this->db,'prevent'=>'ru','db_pref'=>'ru']);
          // $CreateCV->test = 1;
          // $CreateCV->create( $user_id, $this->data['linkedin'] );
        }
      }
    }

    /* set form wizard flag */
    public function extRegAction(){
      if(!$this->inSess('challenge')){
        $_SESSION['start_prof'] = 1;
      }
    }

    /** add new user to DB */
    public function addUser($data,$flag=true){
    		if($flag){
    			if($this->checkUser('email',$this->data['email'])){ echo "E"; exit; }
    		}
        $a = $this->insertData('registerusers',$data);
        if($a){

          $this->insertData('user_balance',array('user_id'=>$a,'balance'=>10));
          $this->insertData('raiteng',array('uid'=>$a));
          $this->updateData('registerusers',array('id'=>$a),array('country'=>$this->area[0]['id']),1);

          $this->detectData($a);

          # reseller program
          if((int)$this->data['dir']>0){
            $date = date("Y-m-d H:i:s");
            $dir = (int)$this->data['dir'];
            $u = $this->getData('registerusers',array('id'),array('id'=>$dir),array(),1);
            if(isset($u[0]['id'])){
              $b = $this->getData('user_balance',array('balance'),array('user_id'=>$dir),array(),1);
              $b = (float)$b[0]['balance']+5;
              $this->updateData('user_balance',array('user_id'=>$dir),array('balance'=>$b),1);
              $this->insertData('user_balance_log',array('user_id'=>$dir,'from'=>7,'coins'=>5,'date'=>$date));
              # save user to resseler list
              include_once "../system/helper/core/reseller.php";
              $reseller = new Reseller(array('db_pref'=>'ru','db'=>$this->db));
              $reseller->add($dir);
            }
          }

          // set up form wizard
          $this->extRegAction();

          // set stat item
          if($this->inSess('auth_stat')){
            include_once '../system/helper/stat/stat_log.php';
            $stat = new StatLog(array('db'=>$this->db,'db_pref'=>'ru'));
          }

          // if this is invited from game user
          if($this->inSess('challenge')) return 'challenge '.$_SESSION['challenge'];
          else return $a;
        }
    }

    public function detectData($uid){
      $_SESSION['uid']=$uid;
      $_SESSION['uname']=$this->data['name'];
      $_SESSION['email']=$this->data['email'];
      $_SESSION['uimg']=$this->data['img'];
      $a = $this->getData('registerusers',array('concat(name," ",surname) as uname','img as uimg'),array('id'=>$uid),array(),1);
      foreach($a[0] as $k=>$v){ $_SESSION[$k]=$v; }
      // test for company
      $c = $this->getData('company',array('id'),array('user_id'=>$uid),array(),1);
      if(isset($c[0]['id'])) $_SESSION['myComp']=$c[0]['id'];
      // sociology
      // $this->checkSociology();
    }

    /** check & prepare $_POST data */
    public function getPost(){
      $d=array();
      foreach($_POST as $k=>$v){ $d[$k]=$v; }
      if(isset($_POST['email'])){
        $d['email'] = mysql_escape_string(trim((string)$_POST['email']));
        $d['date'] = date("Y-m-d");
        $d['img'] = "../images/upload/logo.gif";
        $d['interestings'] = '';
        $d['country'] = 1;
      }
      if(isset($_POST['password'])){
        $d['password'] = trim((string)$_POST['password']);
        if(strlen($d['password'])<8){ echo "pmx"; exit; }
        $d['password'] = mysql_escape_string(md5(sha1($d['password'])));
      }
      return $d;
    }

    /**
    check sociology process for this user
    */
    public function checkSociology(){
      $t = $this->getData('sociology_data',array('id'),array('user_id'=>$_SESSION['uid']),array(),1);
      if(!isset($t[0]['id'])){
        $_SESSION['sociology'] = time();
      }
    }

    /**
    minihelper for check user existing in DB
    @var a - fieldname
    @var b - value
    */
    public function checkUser($a,$b){
      $a = $this->getData('registerusers',array('id'),array("{$a}"=>$b),array(),1);
      if($a[0]['id']) return $a[0]['id'];
    }

    public function getRegion($user_id=false){
      include_once '../system/helper/region.php';
      $c = new region(false,array('db_pref'=>'ru','db'=>$this->db,'prevent'=>'ru'));
      if(!$user_id){
        $this->area = $c->getCountry($c->detect());
      }
    }

    public function add(){
      $data = array(
        'name'=>$this->data['name'],
        'email'=>$this->data['email'],
        'password'=>$this->data['password'],
        'img'=>$this->data['img'],
        'interestings' =>'',
        'country' => 1
      );
      $data['date'] = date("Y-m-d");
      echo $this->addUser($data);
    }

    public function forget(){
      $isReal=$this->getData('registerusers',array('id'),array('email'=>$this->data['email']),array(),1);
      if(isset($isReal[0]['id'])){
        $user_id=(int)$isReal[0]['id'];
        // generate of new password
        $pattern = str_shuffle('ti2es2toan32dsho32wt4ekh78av0e43b3len4dedthei09routst87a0ndi4ngtal5e6ntstobr9i9rnger43yo0uhe8llyeahthetw54oactsfi78894rstcoll6231aborati456ona7ndt655iestosmostrec445568entsi679033ngle45foll56ow7ing6t65325hewil3d4ly454su78cc99es86sfu76lc56lu45blife67vo88lume9twovia3mi');
        $newPass=''; $c=mt_rand(10,25);
        for($i=0;$i<$c;$i++){ $newPass.=$pattern[mt_rand(1,strlen($pattern))-1]; }
        if(strlen($newPass)>=14){
          if($this->updateData('registerusers',array('id'=>$user_id),array('password'=>md5(sha1($newPass))),1)){
            /** this class using swift mailer lib. */
            include_once '../system/helper/massage/massage.php';
            $m=new Massage('recovery',array());
            $m->create(array('theme'=>'Password Recovery','user-password'=>$newPass));
            $res = $m->send(array('to'=>$this->data['email']));
            if($res) echo 'uok';
            else echo 'uns';
            exit;
          }
        }
      } else {echo 'unf';exit;}
      exit;
    }

    /** user auth */
    public function login(){
      $user = $this->getData('registerusers','*',array('email'=>$this->data['email'],'password'=>$this->data['password']),array(),1);
      if(!empty($user) && (int)$user[0]['id']>0){
        $_SESSION['ID'] = $user[0]['id'];
        $_SESSION['uid'] = $user[0]['id'];
		    $_SESSION['email'] = $email;
        $_SESSION['uname'] = $user[0]['name'].' '.$user[0]['surname'];
        $_SESSION['uimg'] = $user[0]['img'];
        // if company owner
        $s = $this->getData('company',array('id'),array('user_id'=>$user[0]['id']),array(),1);
        if(isset($s[0]['id'])) { $_SESSION['myComp']=$s[0]['id']; }
        // lang type
        $this->getRegion($user_id);
        if($this->inSess('challenge')) echo 'challenge '.$_SESSION['challenge'];
        else echo "yes";
        exit;
    	} else { $_SESSION['login_er']=1; echo "no"; exit(); }
      exit;
    }

    public function setParams(){
      $this->toJs(array(),'stopExtentions');
    }

    /* get data for refferal-link registration page */
    public function getUser($id){
      $u = $this->getData('registerusers',['img','name','surname'],array('id'=>$id),array(),1);
      if(isset($u[0]['img'])) return $u[0];
      else return false;
    }

    /** AJAX servers */
    public function parseAjax(){
        $ajax_conf = array(
            'add'=>array('name','email','password','dir'),
            'forget'=>array('email'),
            'login'=>array('email','password')
        );
        if($this->data['get'] && $ajax_conf[$this->data['get']] && $this->inPost($ajax_conf[$this->data['get']])){
            $this->{$this->data['get']}();
        }
    }

    // callback redirect for social auth buttons
    public function manageRedirects(){
      if(isset($_SESSION['auth_stat'])){
        # get redirected page
        $p = $_SESSION['auth_stat']['page'];
        $a = explode('/',$p);
        if(in_array('auth',$a))
          header('Location: ../dashboard/index');
        else
          header('Location: '.$p);
      } else {
        header('Location: ../dashboard/index');
      }
    }

    /* save token to user's token list */
    public function saveToken($user_id){
      if(isset($this->token['value']) && strlen($this->token['value'])>3){
        # get existing tokens of user
        $check = $this->getData('user_token',['id','data','log'],['user_id'=>$user_id],[],1);
        $list = isset($check[0]['data']) ? json_decode($check[0]['data']) : (object)[];
        $list->{$this->token['type']} = $this->token['value'];
        if(isset($check[0]['id'])){
          $this->updateData('user_token',['id'=>$check[0]['id']],['data'=>json_encode($list)],1);
          $this->tokenLog = json_decode($check[0]['log']);
        }
        else {
          $this->insertData('user_token',['user_id'=>$user_id,'data'=>json_encode($list)]);
        }
      }
    }

  }

  $auth = new auth();
  if(isset($core)){
	   $auth->getRegion();
  }

?>
