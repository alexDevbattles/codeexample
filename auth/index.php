<?php
  include_once '../system/init.php';
  include_once 'authClass.php';
  include_once 'icore.php';
?>
<!DOCTYPE html>
<html lang="<?=$loc->lang?>">
<head>
  <meta charset="utf-8"/>
  <title><?=$core->set['page']->cont['title']?></title>
  <meta name="description" content="<?=$core->set['page']->cont['desc']?>"/>
  <meta name="keywords" content="<?=$core->set['page']->cont['keys']?>"/>
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="css/index.min.css?v=2"/>
  <?
    $loc->getSeo();
    $core->set['page']->returnBasics();
  ?>
  <!--[if lt IE 9]>
  <script src="js/ie/respond.min.js" cache="false"></script>
  <script src="js/ie/html5.js" cache="false"></script>
  <script src="js/jquery-1.7.2.min.js" cache="false"></script>
  <script src="js/ie/fix.js" cache="false"></script> <![endif]-->
  <script src="../js/analitics.js?v=2" async="true"></script>
</head>
<body class="pure-g-r">
  <div id="slider-box">

    <header class="header main-header trans-100">
       <div id="main-logo" class="trans-150-all">
          <img src="../images/slides/devlogo/full-150w.png"/>
          <img src="../images/slides/devlogo/circ-70w.png" style="display:none; opacity:0.0;" alt="DevBattles" title="DevBattles"/>
       </div>
       <div id="top-nav" class="hb-panel trans-100-all">
          <a href="../sand/index"><?=$loc->nav->sand?></a>
          <a href="../discussion/index"><?=$loc->nav->diss?></a>
          <a href="../company/index"><?=$loc->nav->comp?></a>
          <a href="../company/vacant"><?=$loc->nav->vacant?></a>
       </div>
       <div id="auth-panel" class="b-b b-r <?if($loc->lang=='en') echo 'en-panel';?>">
           <div id="log-in" class="trans-100-all"><?=$loc->data->head->i[0]?></div>
           <div id="log-in-form" class="index-form <?=$core->lang?>-auth">
               <div class="top-conus rotate-45"></div>
               <form class="form-cont login-form">
                   <input type="email" name="email" id="h-email" placeholder="<?=$loc->data->log->i[0]?>" value="" required/>
                   <input type="password" name="password" id="h-password" placeholder="<?=$loc->data->log->i[1]?>" value="" required/>
                   <input type="submit" class="pure-button button-secondary" id="login-send" value="<?=$loc->data->log->i[2]?>"/>
                   <a href="account-forget" class="auth-forget"><?=$loc->data->log->i[3]?></a>
                   <span class="auth-desc"><?=$loc->data->log->i[4]?></span>
                   <div class="socio-box">
                       <a class="socio-auth" href="socialAuthVk.php"><i class="i fa fa-vk"></i></a>
                       <a class="socio-auth" href="socialAuthFacebook.php"><i class="i fa fa-facebook"></i></a>
                       <a class="socio-auth" href="socialAuthGoogle.php"><i class="i fa fa-google-plus"></i></a>
                       <a class="socio-auth" href="socialAuthGit.php"><i class="i fa fa-github"></i></a>
                       <a class="socio-auth" href="socialAuthTw.php"><i class="i fa fa-twitter"></i></a>
                       <a class="socio-auth" href="socialAuthLinkedIn.php"><i class="i fa fa-linkedin"></i></a>
                   </div>
                   <div class="b-line"></div>
                   <img src="../images/icon-close.png" class="auth-close trans-100-all"/>
               </form>
           </div>
           <span class="or-desc b-b"><?=$loc->data->head->i[1]?></span>
           <div id="sign-in" class="b-b"><?=$loc->data->head->i[2]?></div>
           <div id="sign-in-form" class="index-form <?=$core->lang?>-auth">
             <div class="top-conus rotate-45"></div>
             <div class="form-cont">
               <span class="auth-desc center-desc sign-desc"><?=$loc->data->sign->i[0]?></span>
               <a class="pure-button btn-white new-user" href="registration"><i class="fa fa-group"></i> <?=$loc->data->sign->i[1]?></a>
               <a class="pure-button btn-white new-comp" href="registration-comp"><i class="fa fa-briefcase"></i> <?=$loc->data->sign->i[2]?></a>
               <span class="auth-desc"><?=$loc->data->sign->i[3]?></span>
               <div class="socio-box">
                 <a class="socio-auth" href="socialAuthVk.php"><i class="i fa fa-vk"></i></a>
                 <a class="socio-auth" href="socialAuthFacebook.php"><i class="i fa fa-facebook"></i></a>
                 <a class="socio-auth" href="socialAuthGoogle.php"><i class="i fa fa-google-plus"></i></a>
                 <a class="socio-auth" href="socialAuthGit.php"><i class="i fa fa-github"></i></a>
                 <a class="socio-auth" href="socialAuthTw.php"><i class="i fa fa-twitter"></i></a>
                 <a class="socio-auth" href="socialAuthLinkedIn.php"><i class="i fa fa-linkedin"></i></a>
               </div>
               <div class="b-line"></div>
               <img src="../images/icon-close.png" class="auth-close trans-100-all"/>
             </div>
           </div>
       </div>
       <div id="lang-panel" class="b-b b-r">
            <a class="b-b <?if($loc->lang=='en') echo 'active';?>" href="http://<?=$core->site_name.'/en'.substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-4)?>">En</a>
            <a class="b-b <?if($loc->lang=='ru') echo 'active';?>" href="http://<?=$core->site_name.'/ru'.substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-4)?>">Ru</a>
       </div>
    </header>

    <div class="slider-item sd-init sd-center">
      <span class="sd-caption sd-caption-small"><?=$loc->data->slider->main->i[0]?></span>
      <span class="sd-caption sd-caption-big"><?=$loc->data->slider->main->i[1]?></span>
      <span class="sd-motto"><?=$loc->data->slider->main->i[2]?></span>
      <a class="sd-btn" href="registration-user"><?=$loc->data->slider->main->i[3]?></a>
      <img src="../images/slides/auth/earth.png" class="sd-earth"/>
    </div>
    <div class="slider-item sd-comp sd-left">
      <span class="sd-caption sd-caption-small"><?=$loc->data->slider->comp->i[0]?></span>
      <span class="sd-motto"><?=$loc->data->slider->comp->i[1]?></span>
      <span class="sd-motto"><?=$loc->data->slider->comp->i[2]?></span>
      <span class="sd-motto"><?=$loc->data->slider->comp->i[3]?></span>
      <a class="sd-btn" href="registration-company"><?=$loc->data->slider->comp->i[4]?></a>
      <img src="../images/slides/auth/game-1.png" class="sd-rgame comp-man"/>
      <img src="../images/slides/auth/likelogo.png" class="sd-complogo"/>
    </div>
    <div class="slider-item sd-game sd-center">
      <span class="sd-caption sd-caption-small"><?=$loc->data->slider->game->i[0]?></span>
      <span class="sd-caption sd-caption-small"><?=$loc->nav->tour?></span>
      <span class="sd-motto"><?=$loc->data->slider->game->i[1]?></span>
      <span class="sd-motto"><?=$loc->data->slider->game->i[2]?></span>
      <span class="sd-motto"><?=$loc->data->slider->game->i[3]?></span>
      <a class="sd-btn" href="registration-game"><?=$loc->data->slider->game->i[4]?></a>
      <img src="../images/slides/auth/game-3.png" class="sd-lgame"/>
    </div>
  </div>

  <ul id="main-ul" class="pure-u-20-24 offset-2-24 pure-u-lg-20-24 pure-u-md-24-24 pure-u-sm-24-24">

      <? function Sand($d){ global $icore, $loc;  ?>
      <li class="pure-u-lg-3-6 pure-u-lg-3-6 post-item">
          <i class="fa fa-bullhorn main-ico"></i>
          <a href="../sand/post-<?=$d['id'].'-'.$d['link']?>" class="sand-link"><?=$d['title']?></a>
          <div class="post-details">
            <div class="img-box"><img src="<?=$d['user']['img']?>"/></div>
            <span>
              <i class="fa fa-eye-open"></i> <?=$d['view']?> <?=$icore->view->key.$icore->help->plural($d['view'],$icore->view->one,$icore->view->two,$icore->view->five);?>
            </span>
            <span>
              <i class="fa fa-time"></i> <?=$d['date']?>
            </span>
            <div class="b-line"></div>
            <span class="uname"><?=$d['user']['name']?></span>
          </div>
      </li>
      <? }

      function Tour($d){ global $core, $loc; ?>
      <li class="pure-u-lg-3-6 tour-item">
          <i class="fa fa-gamepad main-ico"></i>
          <div class="win-box">
              <img class="lavrus" src="../images/slides/lavrus.png"/>
              <div class="win-item img-item">
                  <img src="<?=$d['user']['img']?>"/>
              </div>
              <span class="uname"><?=$d['user']['name']?></span>
          </div>
          <div class="user-coins">
              <span>+<?=$d['ball']?> DP</span>
              <a href="registration-game-<?=$d['cat']?>" class="pure-button button-secondary"><?=$loc->data->cont->i[1]?> <?=$d['name']['name']?></a>
          </div>
          <div class="win-info">
              <span><i class="fa fa-time"></i> <?=$d['live_date']?></span>
              <span><?=$d['name']['name']?></span>
          </div>
      </li>
      <? }

      function Discus($d){ global $icore, $loc;?>
      <li class="pure-u-lg-4-6 diss-item">
          <i class="fa fa-group main-ico"></i>
          <a href="../discussion/view-<?=$d['id'].'-'.$icore->url($d['title'])?>" class="sand-link">
             <span>{</span><?=$d['title']?><span>}</span>
             <div class="pure-button button-secondary trans-100"><i class="fa fa-star"></i><?=$loc->data->cont->i[2]?></div>
          </a>
          <div class="diss-details">
              <span><i>+</i><?=$d['comm']?></span>
              <span><?=$icore->anew->key.$icore->help->plural($d['comm'],$icore->anew->one,$icore->anew->two,$icore->anew->five).' '.$icore->answer->key.$icore->help->plural($d['comm'],$icore->answer->one,$icore->answer->two,$icore->answer->five);?></span>
          </div>
          <div class="post-details">
              <div class="img-box"><img src="<?=$d['user']['img']?>"/></div>
              <span>
                  <i class="fa fa-time"></i> <?=$d['date']?>
              </span>
              <div class="b-line"></div>
              <span class="uname"><?=$d['user']['name']?></span>
              <a class="cat-name trans-100" href="../discussion/cat-<?=$d['cat'][0]['cat'].'-'.$icore->url($d['name']['name'])?>"><?=$d['name']['name']?></a>
          </div>
      </li>

      <? }
      function StatSand($d){ global $icore, $loc; ?>
      <li class="pure-u-lg-2-6 stat-item trans-150" a="../sand/cat-<?=$d['cat'].'-'.$icore->url($d['name']['name'])?>">
          <i class="fa fa-signal main-ico"></i>
          <span<? if($d['sm']) echo ' class="sm-stat"';?>><?=$d['name']['name']?></span>
          <span><?=$d['c'].' '.$icore->post->key.$icore->help->plural($d['c'],$icore->post->one,$icore->post->two,$icore->post->five)?></span>
      </li>
      <? }

      function StatDiscus($d){ global $icore, $loc; ?>
      <li class="pure-u-lg-2-6 stat-item trans-150" a="../discussion/cat-<?=$d['cat'].'-'.$icore->url($d['name']['name'])?>">
          <i class="fa fa-signal main-ico"></i>
          <span<? if($d['sm']) echo ' class="sm-stat"';?>><?=$d['name']['name']?></span>
          <span><?=$d['c'].' '.$icore->discus->key.$icore->help->plural($d['c'],$icore->discus->one,$icore->discus->two,$icore->discus->five)?></span>
      </li>
      <? } ?>
      <? $icore->render(); ?>
  </ul>
  <img src="../images/slides/up.png" id="go-top" class="trans-100"/>

  <? $core->toJs($loc->data,'ini_loc'); ?>
  <script src="script/index.min.js?v=2"></script>
</body>
</html>
