<?php

require_once 'module/lib/Splash/LinkedIn/Client.php';
require_once 'module/lib/Splash/LinkedIn/Exception.php';

session_start();

$redirect_uri = 'http://www.devbattles.com/auth/socialAuthLinkedIn.php';
$api_key    = "77xigou9zwd1c4";
$api_secret = "CYKJE7yv2y5qJKt4";

$client = new Splash\LinkedIn\Client($api_key, $api_secret);

if (isset($_GET['logout'])) {
    unset($_SESSION['creds']);
    header("Location: index");
    exit;
}

elseif (isset($_GET['error'])) {
    echo "<h1>ERROR</h1> <p>{$_GET['error_description']}</p>";
}

elseif (isset($_GET['code']) && !isset($_SESSION['creds'])) {
  $access_token = $client->fetchAccessToken($_GET['code'], $redirect_uri);
  $_SESSION['creds'] = $access_token;
}

elseif (!isset($_SESSION['creds'])) {
  $url = $client->getAuthorizationUrl($redirect_uri, sha1(md5("a439iv@8%fdj$3k")."v@8%fd"), "r_basicprofile r_emailaddress");
  header("Location: $url");
}

if(isset($_SESSION['creds'])){

  $client->setAccessToken($_SESSION['creds']['access_token']);
  $response = $client->fetch("/v1/people/~:(id,firstName,lastName,headline,positions:(company,title,summary,startDate,endDate,isCurrent),industry,location:(name,country:(code)),pictureUrl,publicProfileUrl,emailAddress,educations,dateOfBirth)");

  if(isset($response['id'])){
    include_once __DIR__.'/authClass.php';
    $auth->data['name'] =  $response['firstName']." ".$response['lastName'];
    $auth->data['email'] = isset($response['emailAddress']) ? $response['emailAddress'] : '';
    $auth->data['img'] = isset($response['pictureUrl']) ? $response['pictureUrl'] : '';
    $auth->data['date'] = date("Y-m-d");
    $auth->data['linkedin'] = isset($response['publicProfileUrl']) ? $response['publicProfileUrl'] : '';
    $auth->data['linkdn'] = $response['id'];

    /* detect country */
    if(isset($response['location'])){
      $country = $auth->getData('country', ['id'], ['region'=>strtoupper($response['location']['country']['code'])], [], 1 );
      if(isset($country[0]['id'])){
        $auth->data['country'] = $country[0]['id'];
      }
    }
    $auth->socAuth('linkdn', $response['id']);
  }
}
